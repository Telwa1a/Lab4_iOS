//
//  DetailsViewController.swift
//  Labbo4
//
//  Created by DevComputer on 2017-12-01.
//  Copyright © 2017 BrokenDevelopment. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var planet : Planet?
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var rotper: UILabel!
    @IBOutlet weak var orbper: UILabel!
    @IBOutlet weak var diameter: UILabel!
    @IBOutlet weak var climate: UILabel!
    @IBOutlet weak var gravity: UILabel!
    @IBOutlet weak var terrain: UILabel!
    @IBOutlet weak var subwat: UILabel!
    @IBOutlet weak var pop: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = planet?.name
        rotper.text = "Rotation Period: " + (planet?.rotation_period)!
        orbper.text = "Orbital Period: " + (planet?.orbital_period)!
        diameter.text = "Diameter: " + (planet?.diameter)!
        climate.text = "Climate: " + (planet?.climate)!
        gravity.text = "Gravity: " + (planet?.gravity)!
        terrain.text = "Terrain: " + (planet?.terrain)!
        subwat.text = "Surface Water: " + (planet?.surface_water)!
        pop.text = "Population: " + (planet?.population)!
    }
}

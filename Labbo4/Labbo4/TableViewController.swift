//
//  ViewController.swift
//  Labbo4
//
//  Created by DevComputer on 2017-12-01.
//  Copyright © 2017 BrokenDevelopment. All rights reserved.
//

import UIKit

struct Swapi: Codable{
    let results: [Planet]
}

class TableViewController: UITableViewController {

    var planets : [Planet]?
    var planetNumber : Int = -1
    var json: [String: Any]?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? DetailsViewController
        {
            viewController.planet = planets![planetNumber]
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cello", for: indexPath) as! PlanetTableViewCell
        
        if (planets != nil) {
            cell.nameo.text = planets![indexPath.row].name
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (planets == nil) {
            return
        }
        
        DispatchQueue.main.async {
        self.planetNumber = indexPath.row
            self.performSegue(withIdentifier: "getDetails", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://swapi.co/api/planets/?format=json"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let responseData = data else {
                print(error.debugDescription)
                return
            }
            
            do {
                let swapi = try JSONDecoder().decode(Swapi.self, from: responseData)
                DispatchQueue.main.async {
                    self.planets = swapi.results
                    self.tableView.reloadData()
                }
            } catch let jsonError{
                print(jsonError)
                return
            }
            
        }.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }

}


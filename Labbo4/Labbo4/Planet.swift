//
//  Planet.swift
//  Labbo4
//
//  Created by DevComputer on 2017-12-01.
//  Copyright © 2017 BrokenDevelopment. All rights reserved.
//

import Foundation

class Planet : Codable{
    let name: String
    let rotation_period: String
    let orbital_period: String
    let diameter: String
    let climate: String
    let gravity: String
    let terrain: String
    let surface_water: String
    let population: String
}
